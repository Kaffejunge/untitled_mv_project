import torch, torchvision
import detectron2
from detectron2.utils.logger import setup_logger
setup_logger()

# import some common libraries
import numpy as np
import os, json, cv2, random
import time

# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizerLL import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog
from detectron2.structures import pairwise_iou

cfg = get_cfg()
cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_101_FPN_3x.yaml"))
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 6
cfg.MODEL.WEIGHTS =  "Runs\\2022-8-20 2\\output\\model_final.pth"  # path to the model we just trained
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.6   # set a custom testing threshold
predictor = DefaultPredictor(cfg)

from os import walk
from detectron2.utils.visualizerLL import ColorMode
MetadataCatalog.get("LLDataset").set(thing_classes=["Homogeneous Reaction", "Heterogeneous Reaction", "Residue", "Bubble", "Droplet", "Empty"]).set(thing_colors= [(189,16,224), (245,166,35), (110,226,105), (248,231,28), (0,60,255), (200,200,200)])
LLDatasetMetadate = MetadataCatalog.get("LLDataset")

alpha = 1.5 # Contrast control (1.0-3.0)
beta = 0 # Brightness control (0-100)
 # The declaration of CLAHE
# clipLimit -> Threshold for contrast limiting
clahe = cv2.createCLAHE(clipLimit = 4)

# volumeConfidence = 0.7

test_video = r"C:\DeepLearn\LL\EZMax Videos\March 14\UBC-3.mkv"
test_video = r"C:\DeepLearn\LL\EZMax Videos\March 17\UBC-12.mkv"
test_video = r"C:\DeepLearn\LL\EZMax Videos\April 20 2022\UBC-27.mkv"
# test_video = r"C:\DeepLearn\LL\EZMax Videos\August 19 2022\0-80 mL fill in 10 mL increments.mp4"
# test_video = r"C:\DeepLearn\LL\EZMax Videos\August 19 2022\0-80 mL fill in 10 mL increments with mechanical stirring 250 rpm.mp4"
# output_directory = r"C:\DeepLearn\LL\3 EZMax Train V2 with NMS\Model Outputs\R101_FPN\TestRCNNVideo\UBC-bl-26.avi"

vidcap = cv2.VideoCapture(test_video)
frame_width = int(vidcap.get(3))
frame_height = int(vidcap.get(4))
size = (frame_width, frame_height)
success,image = vidcap.read()
# result = cv2.VideoWriter(output_directory,
#                          cv2.VideoWriter_fourcc(*'MJPG'),
#                          30, size)
while success:
  # cv2.imwrite("frame%d.jpg" % count, image)     # save frame as JPEG file
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  success,im = vidcap.read()
  print(vidcap.get(1))
  im = cv2.convertScaleAbs(im, alpha=alpha, beta=beta)
  # lab = cv2.cvtColor(im, cv2.COLOR_BGR2LAB)
  # # lab_planes = cv2.split(lab)
  # lab[:,:,0] = clahe.apply(lab[:,:,0])
  # # lab = cv2.merge(lab_planes)
  # im = cv2.cvtColor(lab, cv2.COLOR_LAB2BGR)
  # cv2.imshow("", im)
  # cv2.waitKey(0)
  #print(os.path.join(test_directory,file))
  start = time.time()
  outputs = predictor(im)  # format is documented at https://detectron2.readthedocs.io/tutorials/models.html#model-output-format
  totalClasses = len(outputs['instances'])
  instanceFilter = np.ones(totalClasses, dtype=bool)

  volumesInstances = outputs["instances"][outputs["instances"].pred_classes < 2]
  totalVolumesInstances = len(volumesInstances)

  if totalVolumesInstances > 0:
    for i in range(0, totalClasses):
      instance = outputs['instances'][i]
      classValue = instance[0].get("pred_classes").cpu().numpy()[0]
      confidenceValue = instance[0].get("scores").cpu().numpy()[0]
      if classValue > 1:
        if classValue==5:
          instanceFilter[i] = False
        continue
      else:
        iouValues = pairwise_iou(instance[0].pred_boxes,
                                 volumesInstances[volumesInstances.scores > confidenceValue].pred_boxes)
        if iouValues.nelement() > 0 and torch.any(iouValues > 0.01):
          instanceFilter[i] = False
        # if confidenceValue < volumeConfidence:
        #   instanceFilter[i] = False

    outputs = {'instances': outputs["instances"][instanceFilter]}

  v = Visualizer(im[:, :, ::-1],
              metadata=LLDatasetMetadate,
              scale=.6,
              instance_mode=ColorMode.SEGMENTATION   # remove the colors of unsegmented pixels. This option is only available for segmentation models
  )
  out = v.draw_instance_predictions(outputs["instances"].to("cpu"))
  end = time.time()

  print(end - start)

  # Color
  boxes = v._convert_boxes(outputs["instances"].pred_boxes.to("cpu")).squeeze()
  mask = outputs['instances'].get('pred_masks').to('cpu')
  num, h, w = mask.shape
  colors = list()

  for i in range(0, num):
    # cv2_imshow(mask[i,:,:].numpy()*255)
    croped = im * np.dstack(([mask[i, :, :].numpy(), mask[i, :, :].numpy(), mask[i, :, :].numpy()]))
    # cv2_imshow(croped)
    cc = croped[:, :, 0]
    averageColorR = np.average(cc[cc != 0], axis=0)
    cc = croped[:, :, 1]
    averageColorG = np.average(cc[cc != 0], axis=0)
    cc = croped[:, :, 2]
    averageColorB = np.average(cc[cc != 0], axis=0)
    # print(averageColor)
    # hsv_Color = colorsys.rgb_to_hsv(averageColorB / 255, averageColorG / 255, averageColorR / 255)
    # print(hsv_Color)
    colors.append(str(int(averageColorR)) +","+ str(int(averageColorG))+","+ str(int(averageColorB)))
    # colors.append(getColor(0))

  if num > 1:
    nn = 0
    for box in boxes:
      
      out = v.draw_text(colors[nn], (box[0] + (box[2] - box[0]) / 2, box[1] + (box[3] - box[1]) / 2))
      nn = nn + 1
  else:
    if num==1:
      out = v.draw_text(colors[0], (boxes[0] + (boxes[2] - boxes[0]) / 2, boxes[1] + (boxes[3] - boxes[1]) / 2))

  # result.write(out.get_image()[:, :, ::-1])
  cv2.imshow('', out.get_image()[:, :, ::-1])
  cv2.waitKey(10)
    # cv2.imwrite(os.path.join(output_directory,filename+".png"), out.get_image()[:, :, ::-1])
# result.release()
vidcap.release()
