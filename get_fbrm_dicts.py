import os, json, cv2
from shapely.geometry.point import Point
import shapely.affinity
import numpy as np
from detectron2.structures import BoxMode

def get_fbrm_dicts(img_dir,categoryNames):
    json_file = os.path.join(img_dir, "via_region_data.json")
    with open(json_file) as f:
        imgs_anns = json.load(f)

    dataset_dicts = []
    for idx, v in enumerate(imgs_anns.values()):
        record = {}

        filename = os.path.join(img_dir, v["filename"])
        # print(v["filename"])
        height, width = cv2.imread(filename).shape[:2]

        record["file_name"] = filename
        record["image_id"] = idx
        record["height"] = height
        record["width"] = width

        annos = v["regions"]
        objs = []
        for anno in annos:
            # assert not anno["region_attributes"]
            category = anno["region_attributes"]
            anno = anno["shape_attributes"]
            name = anno["name"]
            if name == "ellipse":
                cx = anno["cx"]
                cy = anno["cy"]
                rx = anno["rx"]
                ry = anno["ry"]
                theta = anno["theta"]
                ellipse = ((cx, cy), (rx, ry), theta)
                # Let create a circle of radius 1 around center point:
                circ = shapely.geometry.Point(ellipse[0]).buffer(1)
                # Let create the ellipse along x and y:
                ell = shapely.affinity.scale(circ, int(ellipse[1][0]), int(ellipse[1][1]))
                # Let rotate the ellipse (clockwise, x axis pointing right):
                ellr = shapely.affinity.rotate(ell, ellipse[2])

                px, py = ellr.exterior.coords.xy
            elif name == "circle":
                cx = anno["cx"]
                cy = anno["cy"]
                radius = anno["r"]
                circle = ((cx, cy), radius)
                circ = shapely.geometry.Point(circle[0]).buffer(1)
                ell = shapely.affinity.scale(circ, int(circle[1]), int(circle[1]))
                px, py = ell.exterior.coords.xy
            elif name == "polygon":
                px = anno["all_points_x"]
                py = anno["all_points_y"]

            poly = [(x + 0.5, y + 0.5) for x, y in zip(px, py)]
            poly = [p for x in poly for p in x]

            if category["Name"] in categoryNames:
                category_id = categoryNames[category["Name"]]
            else:
                raise Exception(category["Name"] + " Not Found")

            obj = {
                "bbox": [np.min(px), np.min(py), np.max(px), np.max(py)],
                "bbox_mode": BoxMode.XYXY_ABS,
                "segmentation": [poly],
                "category_id": category_id,
            }
            objs.append(obj)
        record["annotations"] = objs
        dataset_dicts.append(record)
    return dataset_dicts
