import os, json
from shapely.geometry.point import Point
import shapely.affinity
import numpy as np
from detectron2.structures import BoxMode

def get_supperannotate_dicts(img_dir, label_dir, categoryNames):
    dataset_dicts = []
    idx = 0
    for r, d, f in os.walk(label_dir):
        for file in f:
            # if file.endswith("___objects.json"):
            if file.endswith(".json"):
                json_file = os.path.join(r, file)
                print(json_file)

                with open(json_file) as f:
                    imgs_anns = json.load(f)

                record = {}
                filename = os.path.join(img_dir, imgs_anns["metadata"]["name"])
                # height, width = cv2.imread(filename).shape[:2]
                record["file_name"] = filename
                record["image_id"] = idx
                record["height"] = imgs_anns["metadata"]["height"]
                record["width"] = imgs_anns["metadata"]["width"]
                idx = idx + 1

                annos = imgs_anns["instances"]
                objs = []
                for anno in annos:
                    categoryName = anno["className"]
                    type = anno["type"]

                    if type == "ellipse":
                        cx = anno["cx"]
                        cy = anno["cy"]
                        rx = anno["rx"]
                        ry = anno["ry"]
                        theta = anno["angle"]
                        ellipse = ((cx, cy), (rx, ry), theta)
                        # Let create a circle of radius 1 around center point:
                        circ = shapely.geometry.Point(ellipse[0]).buffer(1)
                        # Let create the ellipse along x and y:
                        ell = shapely.affinity.scale(circ, int(ellipse[1][0]), int(ellipse[1][1]))
                        # Let rotate the ellipse (clockwise, x axis pointing right):
                        ellr = shapely.affinity.rotate(ell, ellipse[2])

                        px, py = ellr.exterior.coords.xy
                    elif type == "polygon":
                        px = anno["points"][0:-1:2]
                        py = anno["points"][1:-1:2]
                        px.append(anno["points"][0])
                        py.append(anno["points"][-1])
                    poly = [(x + 0.5, y + 0.5) for x, y in zip(px, py)]
                    poly = [p for x in poly for p in x]

                    if categoryName in categoryNames:
                        category_id = categoryNames[categoryName]
                    else:
                        raise Exception(categoryName + " Not Found")
                    obj = {
                        "bbox": [np.min(px), np.min(py), np.max(px), np.max(py)],
                        "bbox_mode": BoxMode.XYXY_ABS,
                        "segmentation": [poly],
                        "category_id": category_id,
                    }
                    objs.append(obj)
                record["annotations"] = objs
                dataset_dicts.append(record)
        return dataset_dicts
