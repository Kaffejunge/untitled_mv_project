import torch, torchvision

print(torch.__version__, torch.cuda.is_available())

# Some basic setup:
# Setup detectron2 logger
import detectron2
from detectron2.utils.logger import setup_logger
setup_logger()

# import some common libraries
import numpy as np
import os, json, cv2, random
from shapely.geometry.point import Point
import shapely.affinity

# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog
from get_supperannotate_dicts import get_supperannotate_dicts
from detectron2.structures import BoxMode
from detectron2.data.datasets import register_coco_instances

# register_coco_instances("LLDataset", {},
#                         r"C:\DeepLearn\LL\3 EZMax Train V2 with NMS\Dataset\Labels May 01 2022\COCO_EZMax.json",
#                         "C:\\DeepLearn\\LL\\3 EZMax Train V2 with NMS\\Dataset\\Train Images April 20+March 21")
#
# MetadataCatalog.get("LLDataset").set(thing_classes=["Homogeneous Reaction", "Heterogeneous Reaction", "Residue", "Bubble", "Droplet"])
torch.cuda.empty_cache()
categoryNames = {
    "Homogeneous reaction": 0,
    "Heterogeneous reaction": 1,
    "Residue above liquid level": 2,
    "Bubble": 3,
    "Droplet": 4,
    "Empty": 5
}

for d in ["Train"]:
    DatasetCatalog.register("LL" + d, lambda d=d: get_supperannotate_dicts(".\\Dataset\\EZMax Aug 21 2022\\"
                                                                             , ".\\Dataset\\EZMax Aug 21 2022\\", categoryNames))

MetadataCatalog.get("LLTrain").set(thing_classes=["Homogeneous Reaction", "Heterogeneous Reaction", "Residue", "Bubble", "Droplet", "Empty"])

LLDatasetMetadate = MetadataCatalog.get("LLTrain")
LLDatasetDict = DatasetCatalog.get("LLTrain")

# dataset_dicts = get_fbrm_dicts("C:\\DeepLearn\\LL\\Dataset")
# for d in dataset_dicts[0:1]:
#     img = cv2.imread(d["file_name"])
#     visualizer = Visualizer(img[:, :, ::-1], metadata=LLDatasetMetadate, scale=1.5)
#     vis = visualizer.draw_dataset_dict(d)
#     cv2.imshow('',vis.get_image()[:, :, ::-1])
#     cv2.waitKey(0)
for d in random.sample(LLDatasetDict, 1):
    img = cv2.imread(d["file_name"])
    visualizer = Visualizer(img[:, :, ::-1], metadata=LLDatasetMetadate, scale=.5)
    vis = visualizer.draw_dataset_dict(d)
    cv2.imshow('',vis.get_image()[:, :, ::-1])
    cv2.waitKey(0)


from detectron2.engine import DefaultTrainer

cfg = get_cfg()
cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_101_FPN_3x.yaml"))
cfg.DATASETS.TRAIN = ("LLTrain")
cfg.DATASETS.TEST = ()
cfg.DATALOADER.NUM_WORKERS = 0
cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-InstanceSegmentation/mask_rcnn_R_101_FPN_3x.yaml")  # Let training initialize from model zoo
# cfg.MODEL.WEIGHTS = os.path.join("C:\\DeepLearn\\LL\\3 EZMax Train V2 with NMS\\Runs\\2022-5-7\\R101_FPN", "model_final.pth")  # path to the model we just trained

cfg.SOLVER.IMS_PER_BATCH = 2
cfg.SOLVER.BASE_LR = 0.00025  # pick a good LR
cfg.SOLVER.MAX_ITER = 100    # 300 iterations seems good enough for this toy dataset; you will need to train longer for a practical dataset
cfg.SOLVER.STEPS = []        # do not decay learning rate
cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 64   # faster, and good enough for this toy dataset (default: 512)
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 6  # only has one class (ballon). (see https://detectron2.readthedocs.io/tutorials/datasets.html#update-the-config-for-new-datasets)
# NOTE: this config means the number of classes, but a few popular unofficial tutorials incorrect uses num_classes+1 here.

os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)
trainer = DefaultTrainer(cfg)
trainer.resume_or_load(resume=True)

trainer.train()